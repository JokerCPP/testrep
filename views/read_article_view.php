<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Панель администратора!</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>style/bootstrap.min.css">  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?=base_url();?>style/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>style/mystyle.css">
    <!-- 1. Подключим библиотеку jQuery (без нее jQuery UI не будет работать) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <!-- 2. Подключим jQuery UI -->
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
</head>


<body>
<div class="conteiner" id="conteiner" >
    <? require_once('application/views/parts/header.php');?>
    <div class="row-fluid">
    <? require_once('application/views/parts/left_syde.php');?>
      
        <div class="col-sm-9 col-md-9 col-lg-9" id="content">
  
   <?php foreach($article as $item):?>
<table class="table table-striped">
 <tr>
    <th colspan="2"><h3><?=$item['title']."<br/>";?></h3></th>
 </tr>
 <tr >
    <td width="20%" rowspan="3"><img width="200px" src="http://yarik.ua/img/<?=$item['picture'];?>"/></td>
    <td width="80%"><p><?php $str = $item['text'];
   if (strlen($str)>100){ 
    $pred = explode('.', $str);
    $str=$pred[0].'.'.$pred[1].'.';}
    echo ($str."<br/><hr>");?></p></td>
 </tr>
 <tr height="10px">
    <td ><a href ="<?php echo base_url().'index.php/read_article/article/'.$item['name']?>" ><p style="text-align: right;">Подробнее</p></a></td>
 <tr height="10px">
    <td class="v_center"><p style="text-align: right;">Статья создана автором: <?=$item['login']?></p><p style="text-align: right;">Дата и время создания: <?=$item['date']?></p></td>
 </tr>  
</table>
<?php endforeach;?>
  </div>
    </div>
    
    <? require_once('application/views/parts/footer.php');?>
      </div>  

</body>

</html>