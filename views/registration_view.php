<!DOCTYPE html>
<html lang="en">
<head>
<?php header("Content-Type: text/html; charset=utf-8"); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>Регистрация нового пользователя</title>
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>style/bootstrap.min.css">  
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<?=base_url();?>style/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>style/mystyle.css">
<!-- 1. Подключим библиотеку jQuery (без нее jQuery UI не будет работать) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<!-- 2. Подключим jQuery UI -->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

<script type="text/javascript"> 
$(document).ready(function(){
    $("#login").focusin(function(){
        if ($("#login").val()=="Введите логин") {
            $("#login").val("");
            }; 
     })
    $("#login").focusout(function(){
        if ($("#login").val()=="") {
            $("#login").val("Введите логин");
            }; 
         })       
    $("#login").focusin(function(){
        if ($("#password").val()=="Введите пароль") {
            $("#password").val("");
            }; 
     })
    $("#password").focusout(function(){
        if ($("#password").val()=="") {
            $("#password").val("Введите пароль");
            }; 
         })  
    $("#but1").click(function(){
        $("#password").css("color","red");
        
    });
    
   $("#email").mouseover(function(){
    $("#email").animate({
         borderColor:"#EA3B53",
         borderWidth:3,
         width:300,
         height:250},1000);

   });
   $("#email").mouseleave(function(){
    $("#email").animate({
         borderColor:"black",
         borderWidth:1,
         width:25,
         height:200},1000);

   });
});
</script> 
</head>
<body>
<div class="conteiner" id="conteiner" >
<? include('application/views/parts/header.php');?>
<div class="row-fluid">
<? include('application/views/parts/left_syde.php');?>
  
    <div class="col-sm-9 col-md-9 col-lg-9" id="content">
       РЕГИСТРАЦИЯ
           <form method="post" action="<?=base_url();?>index.php/registration" >
    <h1>Вы хотите добавить нового пользователя!</h1>
    Добавьте имя пользователя: </br>
    <input type="text" name="login" width="150px" value="<?=set_value('login')?>" /><?=form_error('login')?>
    <br/>
    <hr/>
    Добавьте пароль: </br>
    <input type="text" name="password" value="<?=set_value('password')?>" /><?=form_error('password')?>
    <br/>
    <hr/>
    <input type="submit" name="register" value="Добавить"/>
    </form>
    </div>
</div>

<? include('application/views/parts/footer.php');?>
  </div>  



</body>
</html>