<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Панель администратора!</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>style/bootstrap.min.css">  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?=base_url();?>style/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>style/mystyle.css">
    <!-- 1. Подключим библиотеку jQuery (без нее jQuery UI не будет работать) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <!-- 2. Подключим jQuery UI -->
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
</head>


<body>
<div class="conteiner" id="conteiner" >
    <? require_once('application/views/parts/header.php');?>
    <div class="row-fluid">
    <? require_once('application/views/parts/left_syde.php');?>
      
        <div class="col-sm-9 col-md-9 col-lg-9" id="content">
    <form method="post" action="<?=base_url();?>index.php/upload_article" enctype="multipart/form-data" >
    <h1>Вы хотите добавить новую статью!</h1>
    Добавьте название статьи </br>
    <input type="text" name="title" width="150px" value="<?=set_value('title')?>" /><?=form_error('title')?>
    <br/>
    <hr/>
    Добавьте текст статьи </br>
    <textarea name = "article_text" rows="10" cols="50" value="<?=set_value('article_text')?>" ></textarea><?=form_error('article_text')?>
    <br/>
    <hr/>
    Добавьте изображение статьи </br>
    <input type="file" name="userfile"/>
    
    <br/>
    <hr/>
    <input type="button" id="but1" value="Скрыть"></input>
    <input type="submit" name="download" value="Загрузить"/>
    </form>
    <a href="http://yarik.ua/index.php/blog/adminka">Перейти в админку</a> <br />
    <a href="http://yarik.ua/index.php/blog/read_article">Перейти к чтению статей</a>  	<br />
    <a href="http://yarik.ua/">На главную страницу</a>
        </div>
    </div>
    
    <? require_once('application/views/parts/footer.php');?>
      </div>  

</body>

</html>