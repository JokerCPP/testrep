<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Content-Type: text/html; charset=utf-8"); 
class Rules_model extends CI_Model {

public $add_rules = array(
          array(
             'field'=>'title',
             'label'=>'Название статьи',
             'rules'=>'required|xss_clean|min_lenght[7]|max_lenght[20]|trim'
                        ),
          array(
             'field'=>'article_text',
             'label'=>'Текст статьи',
             'rules'=>'required|xss_clean|min_lenght[7]|max_lenght[2000]|trim'
                        )
);

public $add_user_rules = array(
          array(
             'field'=>'login',
             'label'=>'Логин',
             'rules'=>'required|xss_clean|min_lenght[7]|max_lenght[20]|trim'
                        ),
          array(
             'field'=>'password',
             'label'=>'Пароль',
             'rules'=>'required|xss_clean|min_lenght[7]|max_lenght[2000]|trim'
                        )
);
}
