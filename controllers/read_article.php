<?php
header("Content-Type: text/html; charset=utf-8");	
?><?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Read_article extends CI_Controller {


public function index()
	{
$this->load->model('read_article_model');
$article['article'] = $this->read_article_model->get_article();
$this->load->view('read_article_view',$article);
	}

public function article($name_article){
    $this->load->model('read_article_model');
    $article['article'] = $this->read_article_model->get_article_name($name_article);
    $this->load->view('read_article_name_view',$article);    
}
 
public function remove_article($name_article){
    $this->load->model('read_article_model');
    $this->read_article_model->remove_article($name_article);
    header("location: ".base_url()."/index.php/read_article");
}
 }