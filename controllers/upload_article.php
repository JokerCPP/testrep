<?php
header("Content-Type: text/html; charset=utf-8");	
?><?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_article extends CI_Controller {


 public function index()
	{
 if(isset($_POST['download']))
      {
        $this->load->library('form_validation');
        $this->load->model('rules_model');
        $this->form_validation->set_rules($this->rules_model->add_rules);
        $check = $this->form_validation->run();
        if($check == true)
          {
            $config['upload_path'] = './img/';
         $config['allowed_types'] = 'gif|jpg|png';
	 	 $config['max_size']	= '0';
         $config['encrypt_name'] = true;
         $config['remove_spaces'] = true;
       $this->load->library('upload', $config); 
       $this->upload->do_upload();
       $images_data = $this->upload->data();
       $config = array(
                    'image_library'=>'gd2',
                    'source_image'=>$images_data['full_path'],
                    'new_image'=>APPPATH.'../img/thumbs',
                    'create_thumb'=>true,
                    'maintain_ratio'=>true,
                    'width'=>80,
                    'height'=>80
       );
       
       $this->load->library('image_lib', $config);
       $this->image_lib->resize();
       $add['picture']=$images_data['file_name'];
       $this->load->database();
       $title=$this->input->post('title');
       $autor_id=$this->session->userdata('autor_id');
       $add['autor_id']=$autor_id;
       $add['title']= $title;
       $add['text']=$this->input->post('article_text');
       
       $add['name']=$this->translit($add['title']);
       $this->db->insert('articles',$add);
        echo $autor_id;
        echo "Файл успешно загружен!";
        $this->load->view('adminka_view');
          }
         else {
            $this->load->view('adminka_view');

         }
        
         
      }
      else
        $this->load->view('upload_error');
               
}

 
public function translit($str) {
    $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ');
    $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', '-');
    return str_replace($rus, $lat, $str);
  }
  



 
 }